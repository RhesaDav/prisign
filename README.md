<h1>Prisign</h1>
<hr><p>Sebuah Web App untuk menyelesaikan test di PRIVY</p><h2>General Information</h2>
<hr><ul>
<li>Deployed to http://34.128.119.167:3000/login</li>
</ul><h2>Technologies Used</h2>
<hr><ul>
<li>HTML</li>
</ul><ul>
<li>CSS</li>
</ul><ul>
<li>JavaScript</li>
</ul><ul>
<li>React</li>
</ul><ul>
<li>NodeJS</li>
</ul><ul>
<li>Tailwind</li>
</ul><h2>Features</h2>
<hr><ul>
<li>User can set avatar</li>
</ul><ul>
<li>User can set profile</li>
</ul><ul>
<li>User can add some images and save to gallery</li>
</ul><ul>
<li>User can add history careers</li>
</ul><ul>
<li>User can add history educations</li>
</ul><ul>
<li>Login and Register</li>
</ul><ul>
<li>Send OTP to login</li>
</ul><h5>Steps</h5><ul>
<li>Install NodeJS</li>
</ul><ul>
<li>git clone https://github.com/RhesaDav/prisign.git</li>
</ul><ul>
<li>cd prisign</li>
</ul><ul>
<li>npm install</li>
</ul><ul>
<li>npm start</li>
</ul><h2>Project Status</h2>
<hr><p>Completed</p><h2>Contact</h2>
<hr><p><span style="margin-right: 30px;"></span><a href="https://www.linkedin.com/in/rhesa-davinanto-9762501aa/"><img target="_blank" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/linkedin/linkedin-original.svg" style="width: 10%;"></a><span style="margin-right: 30px;"></span><a href="https://github.com/RhesaDav"><img target="_blank" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/github/github-original.svg" style="width: 10%;"></a></p>
