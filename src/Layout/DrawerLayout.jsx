import React from "react";
import Navbar from "../components/Navbar";
import banner from "../assets/Banner.png";
import ProfileCard from "../components/ProfileCard";
import { useEffect } from "react";
import { baseApi } from "../api/baseApi";
import { useState } from "react";
import {BsCamera} from 'react-icons/bs'
import Loader from "../components/Loader";

export default function DrawerLayout({ children }) {
  const [bannerImage, setBannerImage] = useState(null)
  const [previewBanner, setPreviewBanner] = useState(null)
  const [userData, setUserData] = useState({
    name:'',
    level: 0,
    id: ''
  });
  const [loading,setLoading] =useState(false)

  const loadUserData = async () => {
    setLoading(true)
    console.log(localStorage.getItem("access-token"));
    await baseApi.get("/profile/me").then((res) => {
      localStorage.setItem("userData", JSON.stringify(res.data.data.user));
      setUserData(res.data.data.user);
      console.log('userData', userData)
      const bannerLocalStorage = localStorage.getItem('banner-url')
      setBannerImage(res.data.data.user.cover_picture.url)
      console.log('banner local',bannerLocalStorage)
      setLoading(false)
    }).catch((err) => {
      console.log(err)
      window.location.reload()
    })
  }

  const changeCoverImage = async (e) => {
    e.preventDefault();
    console.log(e)
    setPreviewBanner(URL.createObjectURL(e.target.files[0]))
    await baseApi.post("/uploads/cover", {image:e.target.files[0]}, {headers: {"Content-Type": "multipart/form-data"}}).then((res) => {
      if (res.status === 201) {
        localStorage.setItem('banner-url', res.data.data.user_picture.cover_picture.url)
        loadUserData()
      }
    })
  }

  useEffect(() => {
    if (!localStorage.getItem("access-token")) {
      window.location.href = "/login";
    }
  }, []);

  useEffect(() => {
    loadUserData()
  }, []);
  return (
    <div>
      {loading? <Loader/> : (
      <div>
        <Navbar />
        <div className="static min-h-screen mb-32">
          <label htmlFor="change-cover" className="cursor-pointer absolute top-24 right-7 flex items-center gap-2 bg-gray-200 py-2 px-3 opacity-50 hover:opacity-10 rounded-full hover:bg-gray-100">
            <BsCamera/>
            Change Cover
          </label>
          <input id="change-cover" type="file" onChange={changeCoverImage} className="hidden"/>
          <img src={bannerImage? bannerImage : banner} className="w-full h-80 object-cover" alt="" />
          <div className="absolute top-28 w-full left-5">
            <div className="flex flex-col ml-20 gap-3">
              <h1 className="text-white text-2xl font-semibold mt-5">
                Welcome to Prisign
              </h1>
              <div className="w-2/3">
                <span className="text-white font-semibold text-xl">
                  Is a personal data platform, you can update your information
                  about yourself, customize your profile and change a lot of
                  things.
                </span>
              </div>
            </div>
            <div className="mt-16 flex justify-center gap-20">
              <div>
                <ProfileCard data={userData} />
              </div>
              <div className="bg-white w-2/3 rounded-2xl shadow-md p-5">
                <div className="mb-5">
                  <h1 className="text-4xl font-bold tracking-wide">
                    {userData?.name}
                  </h1>
                  <span className="text-sm font-semibold tracking-wide">
                    Level {userData?.level} #{userData?.id}
                  </span>
                </div>
                {children}
              </div>
            </div>
          </div>
        </div>
      </div>
      )}
    </div>
  );
}
