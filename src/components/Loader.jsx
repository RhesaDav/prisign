import React from "react";
import { ThreeCircles } from "react-loader-spinner";

export default function Loader() {
  return (
    <>
    <div className="fixed flex items-center justify-center w-full h-full top-0 left-0 overflow-hidden bg-gray-500 bg-opacity-25">
    <ThreeCircles
        height="100"
        width="100"
        color="rgb(239 68 68)"
        wrapperStyle={{}}
        wrapperClass=""
        visible={true}
        ariaLabel="three-circles-rotating"
        outerCircleColor=""
        innerCircleColor=""
        middleCircleColor=""
      />
    </div>
    </>
  );
}
