import React from "react";
import { useState } from "react";
import UploadPicture from "./UploadPicture";
import Modal from "./Modal";
import { useEffect } from "react";

export default function ProfileCard({ data }) {
  const [modalIsOpen, setIsOpen] = useState(false);

  const openModal = () => {
    setIsOpen(true);
  };

  function closeModal() {
    setIsOpen(false);
  }

  useEffect(() => {
    console.log(JSON.parse(localStorage.getItem("userData")));
    console.log(data);
  },[])

  return (
    <div className="flex flex-col rounded-2xl p-2 bg-white border gap-2 shadow-md">
      <h1 className="text-left mx-2 text-lg font-semibold">Profile Picture</h1>
      <div className="flex flex-col items-center gap-2">
        <img
          src = {data.user_picture?.picture.url ? data.user_picture.picture.url  : "https://therminic2018.eu/wp-content/uploads/2018/07/dummy-avatar-300x300.jpg"}
          className="w-48 h-48 object-cover rounded-lg"
          alt=""
        />
        <button
          onClick={openModal}
          className="text-red-500 border-red-500 border font-lg rounded-full font-bold w-full py-3 mx-5 hover:bg-red-500 hover:text-white"
        >
          Upload Media
        </button>
        <span className="text-sm text-gray-500">
          PNG, JPG or MP4 up to 50MB
        </span>

        <Modal isOpen={modalIsOpen} onRequestClose={closeModal} modalTitle='Change Avatar'>
          <UploadPicture close={closeModal} />
        </Modal>
      </div>
    </div>
  );
}
