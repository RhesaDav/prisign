import React, { useState } from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";
import logo from "../assets/Logo.png";
import Modal from "./Modal";
import {baseApi} from "../api/baseApi"

export default function Navbar() {
  const navigate = useNavigate()
  const [modal, setModal] = useState(false);

  let activeClassName =
    "underline underline-offset-8 decoration-2 text-blue-500 text-lg";
  let inactiveClassName = "text-gray-500 text-lg";

  const handleModal = () => {
    setModal(!modal);
  };

  const handleLogout = async () => {
    localStorage.removeItem('access-token')
    navigate('/login')
    // const accessToken = localStorage.getItem("access-token");
    // const revokeData = {
    //   access_token: accessToken,
    //   confirm: 1,
    // };
    // await baseApi.post('/oauth/revoke', revokeData).then((res) => {
    //   if(res.status === 201) {
    //     localStorage.clear()
    //   }
    // })
  };

  return (
    <div className="flex justify-between my-6 mx-10">
      <img src={logo} alt="" />
      <div className="flex items-center gap-10">
        <ul className="flex items-center gap-10">
          <li>
            <NavLink
              className={({ isActive }) =>
                isActive ? activeClassName : inactiveClassName
              }
              to="/profile"
            >
              Profile
            </NavLink>
          </li>
          <li>
            <NavLink
              className={({ isActive }) =>
                isActive ? activeClassName : inactiveClassName
              }
              to="/career"
            >
              Career
            </NavLink>
          </li>
          <li>
            <NavLink
              className={({ isActive }) =>
                isActive ? activeClassName : inactiveClassName
              }
              to="/education"
            >
              Education
            </NavLink>
          </li>
          <li>
            <NavLink
              className={({ isActive }) =>
                isActive ? activeClassName : inactiveClassName
              }
              to="/gallery"
            >
              Gallery
            </NavLink>
          </li>
        </ul>
        <button
          onClick={handleModal}
          className="bg-red-500 py-2 px-3 font-bold uppercase rounded-full text-white hover:bg-white hover:border-red-500 hover:border hover:text-red-500"
        >
          Logout
        </button>
        <Modal isOpen={modal} onRequestClose={() => setModal(!modal)}>
          <div className="m-5">
            <div className="flex">
              <h1 className="text-2xl font-bold">
                Are you sure want to logout?
              </h1>
            </div>
            <div className="flex justify-end gap-3 mt-5">
              <button
                onClick={handleModal}
                className="border-2 border-gray-500 text-gray-500 px-4 py-2 text-sm font-bold uppercase rounded-full"
              >
                Cancel
              </button>
              <button
                onClick={handleLogout}
                className="border-2 border-red-500 text-red-500 px-4 py-2 text-sm font-bold uppercase rounded-full"
              >
                Logout
              </button>
            </div>
          </div>
        </Modal>
      </div>
    </div>
  );
}
