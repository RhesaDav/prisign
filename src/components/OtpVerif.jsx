import React from "react";
import { useNavigate } from "react-router-dom";
import Welcome from "../pages/Welcome";
import { useState } from "react";
import { baseApi } from "../api/baseApi";
import OtpInputField from "./OtpInputField";
import { FiRefreshCw } from "react-icons/fi";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function OtpVerif() {
  const navigate = useNavigate();
  const [otp, setOtp] = useState("");

  const otpOnChange = (otp) => {
    setOtp(otp);
  };

  const resendOtp = () => {
    const phone = localStorage.getItem("phone");
    console.log(phone);
    baseApi
      .post("/register/otp/request", {
        phone: phone,
      })
      .then((res) => {
        if (res.status === 201) {
          toast.success(`Success send to ${phone}`, {
            pauseOnHover: false,
          });
        }
      });
  };

  const handleOtp = (e) => {
    e.preventDefault();
    const otpData = {
      user_id: localStorage.getItem("userId"),
      otp_code: otp,
    };
    console.log("OTP", otpData);
    if (otp === "") {
      toast.warning("Please input OTP", {
        pauseOnHover: false,
      });
    } else {
      baseApi.post("/register/otp/match", otpData).then((res) => {
        if (res.status === 201) {
          // localStorage.removeItem("phone");
          navigate("/profile");
          console.log("OTP", res);
        }
      });
    }
  };

  useState(() => {
    baseApi
      .post("/register/otp/request", {
        phone: localStorage.getItem("phone"),
      })
      .then((res) => {
        console.log("OTP", res);
        localStorage.setItem("userId", res.data.data.user.id);
        localStorage.setItem("userData", JSON.stringify(res.data.data.user));
      });
  }, []);
  return (
    <div>
      <ToastContainer />
      <Welcome nav={false}>
        <div className="flex flex-col gap-3">
          <div>
            <h1>OTP Verification</h1>
            <span>Insert OTP code sent to your phone</span>
          </div>
          <div className="flex flex-col gap-3 justify-between">
            <div className="flex justify-center items-center text-center">
              <OtpInputField
                value={otp}
                valueLength={4}
                onChange={otpOnChange}
              />
            </div>
            <button
              onClick={handleOtp}
              className="uppercase bg-red-500 text-white rounded-full py-2"
            >
              Verify
            </button>
          </div>
        </div>
        <div className="absolute">
          <button
            onClick={resendOtp}
            className="flex relative top-64 mt-5 items-center gap-2 font-semibold text-sm text-blue-500 hover:text-blue-700 hover:underline"
          >
            <FiRefreshCw />
            Resend OTP
          </button>
        </div>
      </Welcome>
    </div>
  );
}
