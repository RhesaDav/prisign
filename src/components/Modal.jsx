import React, { useState } from "react";
import ReactModal from "react-modal";
import { IoClose } from "react-icons/io5";

export default function Modal({ isOpen, onRequestClose, children, modalTitle, className }) {
  return (
    <div>
      <ReactModal
        isOpen={isOpen}
        onRequestClose={onRequestClose}
        className={` bg-white transform -translate-x-1/2 -translate-y-1/2 absolute top-1/2 left-1/2 rounded-2xl shadow-xl ${className}`}
        contentLabel="Upload Modal"
        appElement={document.getElementById("root")}
      >
        <div className="m-3">
        <div className="flex justify-between items-center mb-3 px-2">
          <h2>{modalTitle}</h2>
          <IoClose onClick={onRequestClose} className="w-7 h-7 cursor-pointer" />
        </div>
        {children}
        </div>
      </ReactModal>
    </div>
  );
}
