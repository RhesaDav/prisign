import React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { baseApi } from "../api/baseApi";
import { ProgressBar } from "react-loader-spinner";

export default function AddImage({ loadData }) {
  const navigate = useNavigate();
  const [multipleImage, setMultipleImage] = useState([]);
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);

  const changeMultipleImage = (e) => {
    if (e.target.files) {
      const imageArray = Array.from(e.target.files).map((file) => file);
      setMultipleImage((prevImage) => prevImage.concat(imageArray));
    }
  };

  const deleteImageFromArray = (data) => {
    console.log(data);
    setMultipleImage(multipleImage.filter((a) => a.name !== data.name));
  };

  const uploadImage = (e, i) => {
    console.log(multipleImage[i]);
    setLoading(true)
    setTimeout(() => {
    baseApi
      .post(
        "uploads/profile",
        { image: multipleImage[i] },
        {
          headers: { "Content-Type": "multipart/form-data" },
        }
      )
      .then((res) => {
        console.log(res);
        if (res.status === 201) {
          deleteImageFromArray(multipleImage[i])
          setLoading(false);
        }
      })
      .catch((err) => {
      });
    },1000)
  };

  return (
    <div>
      <div>
        <input
          type="file"
          onChange={changeMultipleImage}
          id="upload-image"
          multiple={true}
          className="hidden"
        />
        <div
          id="handleFile"
          className="flex overflow-x-auto space-x-8 border-dashed border-2 w-full h-96"
        >
          {multipleImage.map((image, index) => (
            <div
              key={index}
              className="flex-shrink-0 gap-5 items-center justify-center border p-2 rounded-lg m-5"
            >
              <img
                src={URL.createObjectURL(image)}
                key={index}
                className="object-cover w-48 h-60 rounded-xl"
                alt=""
              />
              <div className="flex justify-center w-full gap-1 my-2 items-center">
                {loading ? (
                <ProgressBar
                height="80"
                width="80"
                ariaLabel="progress-bar-loading"
                wrapperStyle={{}}
                wrapperClass="progress-bar-wrapper"
                borderColor="#F4442E"
                barColor="#51E5FF"
              />
                ) : ( 
                <>  
                  <button
                  className="bg-red-500 hover:bg-red-300 py-3 w-full rounded-l-full font-bold text-md uppercase"
                  onClick={(e) => uploadImage(e, index)}
                >
                  Upload
                </button>
                <button
                  className="bg-gray-500 hover:bg-gray-300 py-3 w-full rounded-r-full font-bold text-md uppercase"
                  onClick={() => deleteImageFromArray(image)}
                >
                  Delete
                </button>
                </>
                )}
              </div>
            </div>
          ))}
        </div>
        <div className="flex justify-center border-dashed border-2 py-5">
          <div className="">
            <label
              htmlFor="upload-image"
              className="bg-red-500 border-white hover:border-red-500 border-2 text-xl font-bold uppercase hover:bg-white cursor-pointer rounded-full py-2 px-3"
            >
              Upload Some Images
            </label>
          </div>
        </div>
      </div>
      {/* <div className="flex justify-between">
        <span className="text-xs">Maximum File Size : 5 Mb</span>
        <span className="text-xs">PNG, JPG or GIF</span>
      </div>
      <button className="w-full border-2 border-red-500 text-red-500 py-2 font-bold uppercase rounded-full hover:bg-red-500 hover:text-white">
        Upload
      </button> */}
    </div>
  );
}
