import React from "react";
import { useMemo } from "react";

export default function OtpInputField({value, valueLength, onChange}) {
    const valueItems = useMemo(() => {
        const valueArray = value.toString().split('');
        const items = [];

        for (let i = 0; i < valueLength; i++) {
            const char = valueArray[i] || '';

            if (char) {
                items.push(char);
            } else {
                items.push('');
            }
        }

        return items;
    }, [value, valueLength]);

    const inputOnChange = (e, index) => {
        const target = e.target;
        const targetValue = target.value;

        if (targetValue.length > 1) {
            return;
        }

        const newValue = value.toString().split('');

        newValue[index] = targetValue;

        onChange(newValue.join(''));

        const nextInput = target.nextElementSibling;

        if (nextInput) {
            nextInput.focus();
        }
    };

    const inputOnKeyDown = (e, index) => {
        const target = e.target;

        if (e.key !== 'Backspace' || target.value !== '') {
            return;
        }

        const prevInput = target.previousElementSibling || null;

        if (prevInput) {
            prevInput.focus();
        }
    };
    
  return (
    <div>
        <div className="flex justify-between items-center">
        {valueItems.map((item, index) => (
            <input
                className="border-2 rounded-xl h-14 w-14 mx-5 text-center text-2xl text-black"
                key={index}
                type="text"
                value={item}
                onChange={(e) => {
                    inputOnChange(e, index);
                }}
                onKeyDown={(e) => {
                    inputOnKeyDown(e, index);
                }}
            />
        ))}
        </div>

      {/* <form className="flex gap-8 justify-between items-center">
        <input
          className="border h-12 w-12 font-bold text-3xl text-center"
          type="password"
          maxLength='1'
        />
        <input
          className="border h-12 w-12 font-bold text-3xl text-center"
          type="password"
          maxLength='1'
        />
        <input
          className="border h-12 w-12 font-bold text-3xl text-center"
          type="password"
          maxLength='1'
        />
        <input
          className="border h-12 w-12 font-bold text-3xl text-center"
          type="password"
          maxLength='1'
        />
        <button>Verif</button>
      </form> */}
    </div>
  );
}
