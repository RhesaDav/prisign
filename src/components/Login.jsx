import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai";
import { useState } from "react";
import Welcome from "../pages/Welcome";
import { baseApi } from "../api/baseApi";
import Loader from "./Loader";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function Login() {
  let navigate = useNavigate();
  const [showPassword, setShowPassword] = useState(false);
  const [latlong, setLatlong] = useState({ lat: 0, long: 0 });
  const [deviceType, setDeviceType] = useState(2);
  const [loading, setLoading] = useState(false);

  const handleLogin = (e) => {
    e.preventDefault();
    setLoading(true);
    const loginData = {
      phone: 62 + e.target[0].value,
      password: e.target[1].value,
      latlong: `${latlong.lat},${latlong.long}`,
      device_token: "123456789",
      device_type: deviceType,
    };
    setTimeout(() => {
      baseApi.post("/oauth/sign_in", loginData).then((res) => {
        if (res.status === 201) {
          console.log("Login", res.data.data);
          localStorage.setItem("access-token", res.data.data.user.access_token);
          localStorage.setItem("phone", loginData.phone);
          navigate("/otp");
        }
      }).catch((err) => {
        toast.error(err.response.data.error.errors[0], {
          pauseOnHover: false
        })
      })
      setLoading(false);
    }, 1000);
  };

  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      setLatlong({
        lat: position.coords.latitude,
        long: position.coords.longitude,
      });
    });
    const media = navigator.userAgent;
    if (media.includes("Mobile")) {
      setDeviceType(1);
    } else if (media.includes("Tablet")) {
      setDeviceType(0);
    } else {
      setDeviceType(2);
    }
  }, []);

  return (
    <div>
      <Welcome nav={true}>
        <ToastContainer/>
        {loading ? (
          <Loader />
        ) : (
          <div className="flex flex-col">
            <h1 className="font-bold text-xl mb-5">Login Account</h1>
            <div className="flex flex-col gap-5">
              <form onSubmit={handleLogin}>
                <div className="flex flex-col">
                  <label className="">Phone Number</label>
                  <input
                    type="text"
                    className="border-blue-400 w-72 border-2 rounded-2xl px-3 py-1"
                    defaultValue="85156397094"
                  />
                </div>
                <div className="flex flex-col">
                  <label>Password</label>
                  <div className="flex items-center">
                    <input
                      type={showPassword ? "text" : "password"}
                      className="border-blue-400 w-72 border-2 rounded-2xl px-3 py-1"
                      defaultValue="12345678"
                    />
                    <div className="relative right-8">
                      {showPassword ? (
                        <AiOutlineEyeInvisible
                          onClick={() => setShowPassword(!showPassword)}
                        />
                      ) : (
                        <AiOutlineEye
                          onClick={() => setShowPassword(!showPassword)}
                        />
                      )}
                    </div>
                  </div>
                </div>
                <div className="flex flex-col gap-3 mt-8">
                  <button className="bg-red-500 text-white p-1 rounded-2xl">
                    Login
                  </button>
                  <button className=" text-gray-400 border p-1 rounded-2xl border-gray-400">
                    Reset
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}
      </Welcome>
    </div>
  );
}
