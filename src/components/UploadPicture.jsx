import React from "react";
import { useState } from "react";
import { BiImageAdd } from "react-icons/bi";
import { useNavigate } from "react-router-dom";
import { baseApi } from "../api/baseApi";

export default function UploadPicture({ close }) {
  const navigate = useNavigate()
  const [file, setFile] = useState(null);
  const [preview, setPreview] = useState(null);

  const handleFile = (e) => {
    setFile(e.target.files[0]);
    setPreview(URL.createObjectURL(e.target.files[0]));
  };

  const handleUploadImage = async (e) => {
    e.preventDefault();
    console.log(e.target[0].files[0]);

    await baseApi
      .post(
        "/uploads/profile",
        {
          image: e.target[0].files[0],
        },
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      )
      .then((res) => {
        if (res.status === 201) {
          baseApi.post("/uploads/profile/default", { id: res.data.data.user_picture.id }).then((res) => {
            console.log(res);
            navigate("/profile");
          });
        }
      });
  };

  return (
    <div>
      <form  className="flex flex-col gap-4 p-2 w-72" onSubmit={handleUploadImage}>
        <div>
          <div className="border-dashed border-2 w-full h-80">
            {file ? (
              <img src={preview} className="w-full h-80 object-cover" alt="" />
            ) : (
              <div className="flex flex-col items-center justify-center h-full">
                <BiImageAdd className="w-10 h-10" />
                <span className="text-black">
                  Drag & Drop your Picture here
                </span>
                <span className="text-black">or</span>
                <label
                  htmlFor="upload-image"
                  className="text-red-500 cursor-pointer hover:text-red-100"
                >
                  Browse file
                </label>
              </div>
            )}
            <input
              type="file"
              onChange={handleFile}
              id="upload-image"
              style={{ display: "none" }}
            />
          </div>
        </div>
        <div className="flex justify-between">
          <span className="text-xs">Maximum File Size : 5 Mb</span>
          <span className="text-xs">PNG, JPG or GIF</span>
        </div>
        <button className="w-full border-2 border-red-500 text-red-500 py-2 font-bold uppercase rounded-full hover:bg-red-500 hover:text-white">
          Upload
        </button>
      </form>
    </div>
  );
}
