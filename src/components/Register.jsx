import React from "react";
import { useNavigate } from "react-router-dom";
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai";
import { useState } from "react";
import { countryCode } from "../country-code.js";
import { useEffect } from "react";
import { baseApi } from "../api/baseApi.js";
import Welcome from "../pages/Welcome.jsx";

export default function Register() {
  let navigate = useNavigate();
  const [showPassword, setShowPassword] = useState(false);
  const [latlong, setLatlong] = useState({ lat: 0, long: 0 });
  const [deviceType, setDeviceType] = useState(2);

  const handleRegister = (e) => {
    e.preventDefault();
    const registerData = {
      country: e.target[0].value,
      phone: 62+e.target[1].value,
      password: e.target[2].value,
      latlong: `${latlong.lat},${latlong.long}`,
      device_token: "123456789",
      device_type: deviceType,
    };
    console.log("Register", registerData);
    // baseApi.post("/register", registerData).then((res) => {
    //   console.log("Register", res);
    //   if (res.data.status === "success") {
    //     navigate("/login");
    //   }
    // });

    // axios.post(`/api/v1/register`, registerData).then((res) => {
    //   console.log("Register", res);
    //   if (res.data.status === "success") {
    //     navigate("/login");
    //   }
    // });
    baseApi.post("/register", registerData).then((res) => {
      console.log("Register", res);
      if (res.status === 200) {
        navigate("/login");
      }
    });
  };

  const handleReset = (e) => {
    e.preventDefault();
    e.target.reset();
  };

  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      setLatlong({
        lat: position.coords.latitude,
        long: position.coords.longitude,
      });
    });
    const media = navigator.userAgent;
    if (media.includes("Mobile")) {
      setDeviceType(1);
    } else if (media.includes("Tablet")) {
      setDeviceType(0);
    } else {
      setDeviceType(2);
    }
  }, []);

  return (
    <div>
      <Welcome nav={true}>
        <div className="flex flex-col gap-5">
          <div>
          <h1 className="font-bold text-xl">Create New Account </h1>
          <span className="text-xs">Before you can join here, please create new account</span>
          </div>
          <form onSubmit={handleRegister}>
            <div className="flex flex-col gap-5">
              <h1 className="text-xl font-bold">Account Detail</h1>
              <div className="flex flex-col gap-2">
                <label className="text-sm">Select Country</label>
                <select name="" id="" className="border-b-2 py-1 px-2">
                  {countryCode.countries.map((country, index) => {
                    return (
                      <>
                        <option
                          hidden
                          selected={index === 103}
                          value={country.name}
                        >
                          {country.name} ({country.code})
                        </option>
                        <option key={index} value={country.name}>
                          {country.name} ({country.code})
                        </option>
                      </>
                    );
                  })}
                  {/* <option value="">Indonesia</option> */}
                </select>
              </div>
              <div className="flex flex-col">
                <label className="">Phone Number</label>
                <input
                  type="number"
                  className="border-blue-400 w-72 border-2 rounded-2xl px-3 py-1"
                  placeholder="822 1234 5678"
                />
              </div>
              <div className="flex flex-col">
                <label>Password</label>
                <div className="flex items-center">
                  <input
                    type={showPassword ? "text" : "password"}
                    className="border-blue-400 w-72 border-2 rounded-2xl px-3 py-1"
                    placeholder="12345678"
                  />
                  <div className="relative right-8">
                    {showPassword ? (
                      <AiOutlineEyeInvisible
                        onClick={() => setShowPassword(!showPassword)}
                      />
                    ) : (
                      <AiOutlineEye
                        onClick={() => setShowPassword(!showPassword)}
                      />
                    )}
                  </div>
                </div>
              </div>
              <div className="flex flex-col gap-3">
                <button className="bg-red-500 text-white p-1 rounded-2xl">
                  Register
                </button>
                <button
                  onClick={handleReset}
                  className=" text-gray-400 border p-1 rounded-2xl border-gray-400"
                >
                  Reset
                </button>
              </div>
            </div>
          </form>
        </div>
      </Welcome>
    </div>
  );
}
