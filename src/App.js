import { Route, Routes, Navigate } from "react-router-dom";
import Career from "./pages/Career";
import Education from "./pages/Education";
import Gallery from "./pages/Gallery";
import Profile from "./pages/Profile";
import EditProfile from "./pages/EditProfile";
import Login from "./components/Login";
import Register from "./components/Register";
import OtpVerif from "./components/OtpVerif";
import DrawerLayout from "./Layout/DrawerLayout";
import Welcome from "./pages/Welcome";

function App() {
  return (
    <div>
      <Routes>
          <Route index element={<Navigate to="/login" />} />
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
          <Route path="otp" element={<OtpVerif />} />
          <Route path="career" element={<Career />} />
          <Route path="education" element={<Education />} />
          <Route path="gallery" element={<Gallery />} />
          <Route path="profile" element={<Profile />} />
          <Route path="edit" element={<EditProfile />} />
      </Routes>
    </div>
  );
}

export default App;
