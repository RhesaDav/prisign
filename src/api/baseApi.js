import axios from "axios";

export const baseApi = axios.create({
    baseURL: '/api/v1',
    headers: {
        'Authorization': localStorage.getItem('access-token'),
        'Accept': 'application/json',
    }
});