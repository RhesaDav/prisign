import React, { useState } from "react";
import DrawerLayout from "../Layout/DrawerLayout";
import { BiEditAlt } from "react-icons/bi";
import { BsCalendar } from "react-icons/bs";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { baseApi } from "../api/baseApi";
import Loader from "../components/Loader";

export default function EditProfile() {
  let navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [date, setDate] = useState(new Date());
  const [userData, setUserData] = useState({
    name: "",
    bio: "",
    level: 1,
    hometown: "",
    gender: "",
    birthday: date,
  });

  const submitEditData = (e) => {
    e.preventDefault();
    setLoading(true);
    const data = {
      name: e.target[0].value,
      bio: e.target[1].value,
      hometown: e.target[2].value,
      gender: e.target[3].value === "Male" ? 0 : 1,
      birthday: new Date(e.target[4].value),
    };
    console.log(data);
    baseApi.post("/profile", data).then((res) => {
      console.log(res);
      localStorage.setItem("userData", JSON.stringify(res.data.data.user));
      navigate("/profile");
    });
    setLoading(false);
  };

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("userData"));
    setUserData(user);
    console.log("userData", userData);
  }, []);

  return (
    <div>
      <DrawerLayout>
        {loading ? (
          <Loader />
        ) : (
          <div className="flex flex-col gap-5">
            <div className="flex items-center justify-between">
              <div>
                <h1 className="text-xl font-bold tracking-normal">
                  Edit Profile Information
                </h1>
                <span className="text-sm text-gray-500">
                  Edit your personal data
                </span>
              </div>
              <BiEditAlt
                className="w-6 h-6 hover:text-red-500 cursor-pointer"
                onClick={() => navigate("/edit")}
              />
            </div>
            <form
              onSubmit={submitEditData}
              className="flex flex-col gap-3 pb-5"
            >
              <h1 className="font-bold text-xl">Information Details</h1>
              <div className="flex flex-col gap-2">
                <label>Name *</label>
                <input
                  className="border-2 rounded-md border-gray-500 py-1 px-3"
                  type="text"
                  defaultValue={userData.name}
                />
              </div>
              <div className="flex flex-col gap-2">
                <label>Bio</label>
                <input
                  className="border-2 rounded-md border-gray-500 py-1 px-3"
                  type="text"
                  defaultValue={userData.bio}
                />
              </div>
              <div className="flex justify-between gap-4">
                <div className="flex flex-col w-1/2 gap-2">
                  <label>Hometown</label>
                  <input
                    className="border-2 rounded-md border-gray-500 py-1 px-3"
                    type="text"
                    defaultValue={userData.hometown}
                  />
                </div>
                <div className="flex flex-col w-1/2 gap-2">
                  <label>Gender</label>
                  <select className="border-b-2 border-gray-500 py-1 px-3">
                    <option value={useEffect.gender} selected hidden>
                      {userData.gender}
                    </option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                  </select>
                </div>
              </div>
              <div className="flex flex-col gap-2">
                <label>Date of Birth</label>
                {/* <input type="date" name="" id="" /> */}
                <div className="flex gap-3 items-center">
                  <BsCalendar />
                  <DatePicker
                    className="border-b-2 border-gray-500 w-full"
                    selected={date}
                    onChange={(date) => setDate(date)}
                  />
                </div>
              </div>
              <div>
                <button>Discard</button>
                <button>Update</button>
              </div>
            </form>
          </div>
        )}
      </DrawerLayout>
    </div>
  );
}
