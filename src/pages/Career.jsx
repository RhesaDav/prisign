import React, { useState } from "react";
import DrawerLayout from "../Layout/DrawerLayout";
import { MdDelete } from "react-icons/md";
import { BsCalendar } from "react-icons/bs";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useNavigate } from "react-router-dom";
import { baseApi } from "../api/baseApi";
import moment from "moment/moment";
import { useEffect } from "react";
import Loader from "../components/Loader";

export default function Career() {
  let navigate = useNavigate();
  const [dateStart, setDateStart] = useState(new Date());
  const [dateEnd, setDateEnd] = useState(new Date());
  const [careerHistory, setCareerHistory] = useState([]);
  const [careerData, setCareerData] = useState([
    {
      employee: "",
      company_name: "",
      ending_in: "",
      starting_from: "",
    },
  ]);
  const [button, setButton] = useState();
  const [loading, setLoading] = useState(false);

  const submitCareerForm = (e) => {
    e.preventDefault();
    if (button === "addCareer") {
      const data = {
        position: "Employee",
        company_name: e.target[0].value,
        starting_from: moment(dateStart).format("MMM D,YYYY"),
        ending_in: moment(dateEnd).format("MMM D,YYYY"),
      };
      console.log(data);
      if (data.company_name === "") {
        console.log("failed");
      } else {
        setLoading(true);
        baseApi.post("/profile/career", data).then((res) => {
          console.log(res.data.data.user.career);
          setCareerHistory([...careerHistory, res.data.data.user.career]);
          localStorage.setItem(
            "career",
            JSON.stringify([...careerHistory, res.data.data.user.career])
          );
          const data = JSON.parse(localStorage.getItem("career"));
          setCareerData(data);
          loadData();
          setLoading(false);
        });
      }
    } else if (button === "discard") {
      e.target[0].value = "";
      e.target[1].value = moment(new Date()).format("MM/DD/YYYY");
      e.target[2].value = moment(new Date()).format("MM/DD/YYYY");
    }
  };

  const deleteCareer = (index) => {
    setLoading(true);
    setTimeout(() => {
      const data = JSON.parse(localStorage.getItem("career"));
      if (index > -1) {
        data.splice(index, 1);
        localStorage.setItem("career", JSON.stringify(data));
        const parse = JSON.parse(localStorage.getItem("career"));
        setCareerData(parse);
        setLoading(false);
      }
    }, 1000);
  };

  const loadData = () => {
    if (localStorage.getItem("career")) {
      const data = JSON.parse(localStorage.getItem("career"));
      setCareerData(data);
    }
  };

  useEffect(() => {
    loadData();
  }, []);

  return (
    <div>
      <DrawerLayout>
        {loading ? (
          <Loader />
        ) : (
          <div className="flex flex-col gap-5">
            <div className="flex items-center justify-between">
              <div>
                <h1 className="text-xl font-bold tracking-normal">
                  Career Information
                </h1>
                <span className="text-sm text-gray-500">
                  Information about your career
                </span>
              </div>
            </div>
            <form
              onSubmit={submitCareerForm}
              className="flex flex-col gap-3 pb-5"
            >
              <div className="flex flex-col gap-2">
                <label>Company Name</label>
                <input
                  className="border-2 rounded-md border-gray-500 py-1 px-3"
                  type="text"
                />
              </div>
              <div className="flex flex-col gap-2">
                <label>Start From</label>
                <div className="flex gap-3 items-center">
                  <BsCalendar />
                  <DatePicker
                    className="border-b-2 border-gray-500 w-full"
                    selected={dateStart}
                    onChange={(date) => setDateStart(date)}
                  />
                </div>
              </div>
              <div className="flex flex-col gap-2">
                <label>Ending In</label>
                <div className="flex gap-3 items-center">
                  <BsCalendar />
                  <DatePicker
                    className="border-b-2 border-gray-500 w-full"
                    selected={dateEnd}
                    onChange={(date) => setDateEnd(date)}
                    minDate={dateStart}
                  />
                </div>
              </div>
              <div className="flex my-3 gap-3">
                <button
                  onClick={() => setButton("discard")}
                  className="border-2 border-gray-500 text-gray-500 px-4 py-2 text-sm font-bold uppercase rounded-full"
                >
                  Discard
                </button>
                <button
                  onClick={() => setButton("addCareer")}
                  className="border-2 border-red-500 text-red-500 px-4 py-2 text-sm font-bold uppercase rounded-full"
                >
                  Add Career
                </button>
              </div>
              <div className="flex flex-col gap-2">
                {careerData.length > 0 ? (
                  careerData
                    .map((item, index) => (
                      <div
                        key={index}
                        className="flex items-center justify-between border-b-2 border-gray-500"
                      >
                        <div className="flex flex-col">
                          <h1 className="font-semibold uppercase text-lg">
                            {item?.company_name}
                          </h1>
                          <span className="text-gray-500 text-md">
                            {/* {moment(item?.starting_from).format("MMM D,YYYY")} -{" "}
                          {moment(item?.ending_in).format("MMM D,YYYY")} */}
                            {item.starting_from} - {item.ending_in}
                          </span>
                        </div>
                        <div className="text-gray-500 hover:text-gray-700">
                          <MdDelete
                            onClick={() => deleteCareer(index)}
                            className="w-10 h-10 cursor-pointer"
                          />
                        </div>
                      </div>
                    ))
                    .reverse()
                ) : (
                  <div className="flex justify-center">
                    <h1 className="text-2xl uppercase font-semibold">
                      Please add some career information
                    </h1>
                  </div>
                )}
              </div>
            </form>
          </div>
        )}
      </DrawerLayout>
    </div>
  );
}
