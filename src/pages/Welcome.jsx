import React, { useState } from "react";
import logo from "../assets/Logo.png";
import masmas from "../assets/Device.png";
import { useEffect } from "react";
import moment from "moment/moment";
import { NavLink, useNavigate } from "react-router-dom";

export default function Welcome({ children, nav }) {
  const navigate = useNavigate()
  const [today, setToday] = useState("");

  let activeClassName = "border-red-700 text-red-700";
  let inactiveClassName = "";

  const handleDate = () => {
    const date = moment().format("MMM DD, YYYY");
    setToday(`Today ${date}`);
  };

  useEffect(() => {
    handleDate();
  }, []);
  
  const navWelcome = () => {
    return (
      <div className="grid grid-cols-2 w-full items-center">
      <NavLink to="/login"
        className={({ isActive }) =>
        `border-b-2 text-center ${isActive ? activeClassName : inactiveClassName}`
        }
      >
        Login
      </NavLink>
      <NavLink to={"/register"}
        className={({ isActive }) =>
        `border-b-2 text-center ${isActive ? activeClassName : inactiveClassName}`
      }
      >
        Registration
      </NavLink>
    </div>
    )
  }

  return (
    <div className="">
      <div className="grid grid-cols-2">
        <div className="m-16 flex flex-col justify-center">
          <img src={logo} alt="logo" className="w-52" />
          <div className="flex flex-col gap-2 m-10">
            <div className="font-bold text-3xl">Welcome to Prisign</div>
            <div className="text-lg">
              Is a personal data platform, you can update your information about
              yourself, customize your profile and change a lot of things.
            </div>
          </div>
          <img src={masmas} className="w-84 mx-24" alt="" />
        </div>
        <div className="flex flex-col gap-3 items-center justify-center">
          <div className="flex flex-col gap-3 shadow-xl py-14 px-10 w-96">
            <h1 className="font-bold">{today}</h1>
            {nav ? navWelcome() : null}
            {/* <div className="grid grid-cols-2 w-full items-center">
              <NavLink to="/login"
                className={({ isActive }) =>
                `border-b-2 text-center ${isActive ? activeClassName : inactiveClassName}`
                }
              >
                Login
              </NavLink>
              <NavLink to={"/register"}
                className={({ isActive }) =>
                `border-b-2 text-center ${isActive ? activeClassName : inactiveClassName}`
              }
              >
                Registration
              </NavLink>
            </div> */}
            {children}
          </div>
        </div>
      </div>
    </div>
  );
}
