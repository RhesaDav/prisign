import React, { useState } from "react";
import DrawerLayout from "../Layout/DrawerLayout";
import { BiEditAlt } from "react-icons/bi";
import { BsCalendar } from "react-icons/bs";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import Loader from "../components/Loader";

export default function Profile() {
  let navigate = useNavigate();
  const [date, setDate] = useState(new Date());
  const [userData, setUserData] = useState({
    // name: "",
    // bio: "-",
    // hometown: "-",
    // gender: "male",
    // level: 0,
    // birthday: date,
  });
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    const user = JSON.parse(localStorage.getItem("userData"));
    console.log(user);
    setUserData({
      name: user.name,
      bio: user.bio,
      hometown: user.hometown,
      gender: user.gender,
      level: user.level,
      birthday: user.birthday,
    });
    setLoading(false)
  }, []);
  return (
    <div>
      <DrawerLayout>
        {loading? <Loader/> : (
        <div className="flex flex-col gap-5">
          <div className="flex items-center justify-between">
            <div>
              <h1 className="text-xl font-bold tracking-normal">
                Profile Information
              </h1>
              <span className="text-sm text-gray-500">Your personal data</span>
            </div>
            <BiEditAlt
              className="w-6 h-6 hover:text-red-500 cursor-pointer"
              onClick={() => navigate("/edit")}
            />
          </div>
          <div className="flex flex-col gap-3 pb-5">
            <h1 className="font-bold text-xl">Information Details</h1>
            <div className="flex flex-col gap-2">
              <label>Name *</label>
              <input
                className="border-2 rounded-md border-gray-500 py-1 px-3"
                type="text"
                defaultValue={userData.name}
                readOnly
              />
            </div>
            <div className="flex flex-col gap-2">
              <label>Bio</label>
              <textarea className="border-2 rounded-md border-gray-500 py-1 px-3 h-32" defaultValue={userData.bio} readOnly/>
                {/* {userData.bio} */}
              {/* </textarea>s */}
              {/* <input
                className="border-2 rounded-md border-gray-500 py-1 px-3 h-20"
                type="text"
                defaultValue={userData.bio}
                readOnly
              /> */}
            </div>
            <div className="flex justify-between gap-4">
              <div className="flex flex-col w-1/2 gap-2">
                <label>Hometown</label>
                <input
                  className="border-2 rounded-md border-gray-500 py-1 px-3"
                  type="text"
                  defaultValue={userData.hometown}
                  readOnly
                />
              </div>
              <div className="flex flex-col w-1/2 gap-2">
                <label>Gender</label>
                <select disabled className="border-b-2 border-gray-500 py-1 px-3">
                  <option value={userData.gender} hidden selected>{userData.gender? userData.gender: 'please select a gender'}</option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                </select>
              </div>
            </div>
            <div className="flex flex-col gap-2">
              <label>Date of Birth</label>
              {/* <input type="date" name="" id="" /> */}
              <div className="flex gap-3 items-center">
                <BsCalendar />
                <DatePicker
                  className="border-b-2 border-gray-500 w-full"
                  selected={date}
                  onChange={(date) => setDate(date)}
                  disabled
                />
              </div>
            </div>
          </div>
        </div>
        )}
      </DrawerLayout>
    </div>
  );
}
