import React, { useState } from "react";
import DrawerLayout from "../Layout/DrawerLayout";
import { MdDelete } from "react-icons/md";
import { BsCalendar } from "react-icons/bs";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import moment from "moment";
import { baseApi } from "../api/baseApi";
import Loader from "../components/Loader";

export default function Education() {
  let navigate = useNavigate();
  const [dateStart, setDateStart] = useState(new Date());
  const [loading, setLoading] = useState(false);
  const [educationHistory, setEducationHistory] = useState([]);
  const [educationData, setEducationData] = useState([
    {
      school_name: "",
      graduation_time: "",
    },
  ]);
  const [button, setButton] = useState("");

  const submitEducationForm = (e) => {
    e.preventDefault();
    setLoading(true);
    if (button === "addEducation") {
      const data = {
        school_name: e.target[0].value,
        graduation_time: moment(dateStart).format("MMM D, YYYY"),
      };
      console.log(data);
      if (data.school_name === "") {
        alert("failed");
      } else {
        baseApi.post("/profile/education", data).then((res) => {
          setEducationHistory([
            ...educationHistory,
            res.data.data.user.education,
          ]);
          localStorage.setItem(
            "education",
            JSON.stringify([...educationHistory, res.data.data.user.education])
          );
          const data = JSON.parse(localStorage.getItem("education"));
          setEducationData(data);
          setLoading(false);
        });
      }
    } else if (button === "discard") {
      e.target[0].value = "";
      e.target[1].value = moment(new Date()).format("MM/DD/YYYY");
    }
  };

  const deleteEducation = (index) => {
    setLoading(true);
    setTimeout(() => {
      const data = JSON.parse(localStorage.getItem("education"));
      if (index > -1) {
        data.splice(index, 1);
        localStorage.setItem("education", JSON.stringify(data));
        const parse = JSON.parse(localStorage.getItem("education"));
        setEducationData(parse);
        setLoading(false);
      }
    }, 1000);
  };

  const loadData = () => {
    if (localStorage.getItem("education")) {
      const data = JSON.parse(localStorage.getItem("education"));
      setEducationData(data);
    }
  };

  useEffect(() => {
    loadData();
  }, []);
  return (
    <div>
      <DrawerLayout>
        {loading ? (
          <Loader />
        ) : (
          <div className="flex flex-col gap-5">
            <div className="flex items-center justify-between">
              <div>
                <h1 className="text-xl font-bold tracking-normal">
                  Education Information
                </h1>
                <span className="text-sm text-gray-500">
                  Information about your Education
                </span>
              </div>
            </div>
            <div className="flex flex-col gap-3 pb-5">
              <form
                className="flex flex-col gap-3 pb-5"
                onSubmit={submitEducationForm}
              >
                <div className="flex flex-col gap-2">
                  <label>School Name</label>
                  <input
                    className="border-2 rounded-md border-gray-500 py-1 px-3"
                    type="text"
                  />
                </div>
                <div className="flex flex-col gap-2">
                  <label>Graduation Time</label>
                  {/* <input type="date" name="" id="" /> */}
                  <div className="flex gap-3 items-center">
                    <BsCalendar />
                    <DatePicker
                      className="border-b-2 border-gray-500 w-full"
                      selected={dateStart}
                      onChange={(date) => setDateStart(date)}
                    />
                  </div>
                </div>
                <div className="flex my-3 gap-3">
                  <button
                    onClick={() => setButton("discard")}
                    className="border-2 border-gray-500 text-gray-500 px-4 py-2 text-sm font-bold uppercase rounded-full"
                  >
                    Discard
                  </button>
                  <button
                    onClick={() => setButton("addEducation")}
                    className="border-2 border-red-500 text-red-500 px-4 py-2 text-sm font-bold uppercase rounded-full"
                  >
                    Add Education
                  </button>
                </div>
              </form>

              <div className="flex flex-col gap-2">
                {educationData.length > 0 ? (
                  educationData
                    .map((item, index) => (
                      <div
                        key={index}
                        className="flex items-center justify-between border-b-2 border-gray-500"
                      >
                        <div className="flex flex-col">
                          <h1 className="font-semibold uppercase text-lg">
                            {item?.school_name}
                          </h1>
                          <span className="text-gray-500 text-md">
                            {/* {moment(item?.starting_from).format("MMM D,YYYY")} -{" "}
                      {moment(item?.ending_in).format("MMM D,YYYY")} */}
                            {item.graduation_time}
                          </span>
                        </div>
                        <div className="text-gray-500 hover:text-gray-700">
                          <MdDelete
                            onClick={() => deleteEducation(index)}
                            className="w-10 h-10 cursor-pointer"
                          />
                        </div>
                      </div>
                    ))
                    .reverse()
                ) : (
                  <div className="flex justify-center">
                    <h1 className="text-2xl uppercase font-semibold">
                      Please add some career information
                    </h1>
                  </div>
                )}
              </div>
            </div>
          </div>
        )}
      </DrawerLayout>
    </div>
  );
}
