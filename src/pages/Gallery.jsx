import React, { useState } from "react";
import DrawerLayout from "../Layout/DrawerLayout";
import { GrAdd } from "react-icons/gr";
import "react-datepicker/dist/react-datepicker.css";
import Modal from "../components/Modal";
import UploadPicture from "../components/UploadPicture";
import { useEffect } from "react";
import { baseApi } from "../api/baseApi";
import { useNavigate } from "react-router-dom";
import AddImage from "../components/AddImage";

export default function Gallery() {
  const navigate = useNavigate();
  const [modal, setModal] = useState(false);
  const [modalImage, setModalImage] = useState(false);
  const [images, setImages] = useState([]);
  const [openImage, setOpenImage] = useState({});
  const [loading, setLoading] = useState(false);

  const modalHandler = () => {
    getData();
    setModal(!modal);
  };

  const openModalImage = (event, data) => {
    setModalImage(!modalImage);
    setOpenImage(data);
  };

  const handleChangeAvatar = (id) => {
    const formData = {
      id: id,
    };
    baseApi.post(`/uploads/profile/default`, formData).then((res) => {
      navigate("/profile");
    });
  };

  const getData = async () => {
    setLoading(true)
    const parse = JSON.parse(localStorage.getItem("userData"));
    setLoading(true);
    setImages(parse.user_pictures);
    setLoading(false);
    setLoading(false)
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <DrawerLayout>
        <div className="flex flex-col gap-5">
          <div className="flex items-center justify-between">
            <div>
              <h1 className="text-xl font-bold tracking-normal">
                Gallery Information
              </h1>
              <span className="text-sm text-gray-500">
                Information about your gallery
              </span>
            </div>
            <GrAdd onClick={modalHandler} className="w-6 h-6 cursor-pointer" />
          </div>
          <div className="grid grid-cols-3 gap-5 px-3 pb-5">
            {images
              .map((image) => (
                <div className="relative">
                  {loading === true ? (
                    <div className="animate-pulse bg-gray-300 h-40 w-full"></div>
                  ) : (
                    <img
                      src={image.picture.url}
                      placeholder="https://media2.giphy.com/media/3oEjI6SIIHBdRxXI40/200w.gif?cid=82a1493bnzd7yt1mt5hc2e09c4k6d3njc76pdcmopn7cdzk1&rid=200w.gif&ct=g"
                      alt=""
                      onClick={(event) => openModalImage(event, image)}
                      className="w-full h-72 object-cover rounded-2xl cursor-pointer transition ease-in-out delay-100 hover:-translate-y-1 duration-300 hover:scale-110"
                    />
                  )}
                </div>
              ))
              .reverse()}
          </div>
        </div>
      </DrawerLayout>

      <Modal
        onRequestClose={() => setModal(!modal)}
        isOpen={modal}
        modalTitle="Upload Image To Gallery"
        className={`w-10/12`}
      >
        <AddImage close={modalHandler} loadData={getData} />
      </Modal>

      <Modal
        onRequestClose={() => setModalImage(!modalImage)}
        isOpen={modalImage}
        modalTitle="Choose Avatar"
      >
        <div>
          <img
            src={openImage.picture?.url}
            className="h-72 w-72 object-cover"
            alt=""
          />
          <div className="flex justify-center items-center gap-8">
            <button
              className="w-full border-2 border-red-500 text-red-500 py-2 font-bold uppercase rounded-full hover:bg-red-500 hover:text-white"
              onClick={() => handleChangeAvatar(openImage.id)}
            >
              Set As Avatar
            </button>
          </div>
        </div>
      </Modal>
    </div>
  );
}
